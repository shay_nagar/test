def receipt(array):
  sum = 0.0
  for i in range(len(array)):
    sum = sum + array[i]
  return sum

array = []

while True:
  print("Calculating Price")
  n = input("Enter a price: ")
  if n == "":
    print("Goodbye")
    break
  array.append(n)
  totalCost = receipt(n)
  print("The total cost is " + str(totalCost))

